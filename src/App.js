import React from 'react';
import './assets/scss/styles.scss'
import Home from "./pages/home";
import NewsFeeds from "./pages/newsFeeds";
import DetailPage from "./pages/detailPage";
import Contact from "./pages/contact";
import ErrorPage from "./pages/404";

import {Route, Switch} from "react-router-dom";

import Navbar from "./components/navbar/navbar";

function App() {
    return (
        <>
            <Navbar/>
            <Switch>
                <Route exact path="/" component={Home}/>
                <Route exact path="/news" component={NewsFeeds}/>
                <Route exact path="/news/:page" component={DetailPage}/>
                <Route exact path="/contact" component={Contact}/>
                <Route component={ErrorPage}/>
            </Switch>
        </>
    );
}

export default App;
