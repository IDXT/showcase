import React, {Component} from "react";
import {FaEnvira, FaGratipay, FaPagelines, FaTheRedYeti} from "react-icons/fa";
import Title from "../title/title";
import "./services.scss"


export default class Services extends Component{

    state={
        services: [
            {
                icon:<FaEnvira/>,
                title:"title 1",
                info:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita, maiores?"
            },
            {
                icon:<FaGratipay/>,
                title:"title 1",
                info:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita, maiores?"
            },
            {
                icon:<FaPagelines/>,
                title:"title 1",
                info:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita, maiores?"
            },
            {
                icon:<FaTheRedYeti/>,
                title:"title 1",
                info:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita, maiores?"
            }
        ]
    };

    className() {
        let number = this.state.services.length;
        if (number === 2) {
            return "services-block col-6";
        } else if (number === 3) {
            return "services-block col-4";
        } else if (number === 4) {
            return "services-block col-3";
        } else {
            return "services-block col-4";
        }
    }

    render() {
        return (
            <section className="services">
                <div className="container">
                    <Title title="Services" />
                    <div className="row">
                        {this.state.services.map((item, index) => {
                            return (
                                <article key={index} className={this.className()}>
                                    <span>{item.icon}</span>
                                    <h3>{item.title}</h3>
                                    <p>{item.info}</p>
                                </article>
                            )
                        })}
                    </div>
                </div>
            </section>
        )
    }
}