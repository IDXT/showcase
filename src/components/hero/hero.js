import React from "react";
import "./hero.scss";

export default function Hero({children, hero}) {
    return (
        <div className="container">
            <header className={hero}>{children}</header>
        </div>
    )
}

Hero.defaultProps = {
    hero: "defaultHero"
};