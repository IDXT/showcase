import React, {Component} from "react";
import "./navbar.scss"
import Logo from "../../assets/images/logo.png"
import {FaAlignRight} from "react-icons/fa";
import {Link} from "react-router-dom";

export default class Navbar extends Component {
    state = {
        isOpen: false
    };

    handelToggle = () => {
        this.setState({isOpen: !this.state.isOpen})
    };

    render() {
        return (
            <nav className="navbar">
                <div className="container">
                    <div className="row">
                    <div className="nav-header">
                        <Link to="/">
                            <img src={Logo} alt=""/>
                        </Link>
                        <button type="button" className="nav-btn" onClick={this.handelToggle}>
                            <FaAlignRight className="nav-icon"/>
                        </button>
                    </div>
                    <ul className={this.state.isOpen ? "nav-links show-nav" : "nav-links"}>
                        <li>
                            <Link to="/">Home</Link>
                        </li>
                        <li>
                            <Link to="/news">News</Link>
                        </li>
                        <li>
                            <Link to="/contact">Contact</Link>
                        </li>
                    </ul>
                    </div>
                </div>
            </nav>
        )
    }
}