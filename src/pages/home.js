import React from "react";
import Hero from "../components/hero/hero";
import Banner from "../components/banner/banner";
import {Link} from "react-router-dom"
import Services from "../components/services/services";

export default function Home() {
    return (
        <>
            <Hero>
                <img src="https://cdn.expatica.com/production/sites/3/2014/05/14184700/Health-Insurance-1920x1080.jpg"
                     alt=""/>
                <Banner title="Unive verzekeringen" subtitle="Unive zorgverzekering">
                    <Link to="/news" className="btn btn-1">
                        News
                    </Link>
                </Banner>
            </Hero>
            <Services>
            </Services>
        </>
    )
};

