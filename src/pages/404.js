import React from "react";
import Hero from "../components/hero/hero";
import {Link} from "react-router-dom";
import Banner from "../components/banner/banner";


export default function ErrorPage() {
    return (
        <Hero>
            <img src="https://www.dutchcowboys.nl/uploads/images/error-404.jpg" alt=""/>
            <Banner title="404" subtitle="page not found">
                <Link to="/" className="btn btn-1">
                    Home
                </Link>
            </Banner>
        </Hero>
    )
};