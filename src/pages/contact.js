import React from "react";
import Hero from "../components/hero/hero";

export default function Contact() {
    return (
        <Hero hero="smallHero">
            <img src="https://nbccongrescentrum.nl/content/uploads/2018/05/Information-overload-op-je-event-zo-voorkom-je-het-1200x720.jpg" alt=""/>

        </Hero>
    )
}